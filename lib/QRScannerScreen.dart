import 'package:flutter/material.dart';
import 'package:flutter_login_layout/qr_scanner.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';

class QRScannerScreen extends StatefulWidget {
  const QRScannerScreen({Key? key}) : super(key: key);

  @override
  _QRScannerScreenState createState() => _QRScannerScreenState();
}

class _QRScannerScreenState extends State<QRScannerScreen> {
  final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');
  late QRViewController controller;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Escáner QR"),
      ),
      body: QRView(
        key: qrKey,
        onQRViewCreated: _onQRViewCreated,
      ),
    );
  }

  void _onQRViewCreated(QRViewController controller) {
    this.controller = controller;
    controller.scannedDataStream.listen((scanData) {
      try {
        if (scanData != null && scanData.code != null) {
          controller.stopCamera();

          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => QRCodeWidget(scanData.code),
            ),
          );
        }
      } catch (e) {
        print("Error al realizar pop: $e");
      }
    });
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }
}
