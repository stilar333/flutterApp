import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';
import 'package:flutter/material.dart';
import 'package:google_mlkit_text_recognition/google_mlkit_text_recognition.dart';
import 'package:mask_for_camera_view/inside_line/mask_for_camera_view_inside_line.dart';
import 'package:mask_for_camera_view/mask_for_camera_view.dart';
import 'package:mask_for_camera_view/mask_for_camera_view_result.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:flutter/painting.dart';
import 'package:path_provider/path_provider.dart';

class CameraScreen extends StatefulWidget {
  @override
  _CameraScreenState createState() => _CameraScreenState();
}

class _CameraScreenState extends State<CameraScreen> {
  late Future<List> _cameraInitialization;

  @override
  void initState() {
    super.initState();
    print('Calling initializeCamera...');
    _cameraInitialization = initializeCamera();
  }

  Future<List> initializeCamera() async {
    WidgetsFlutterBinding.ensureInitialized();
    try {
      return await MaskForCameraView.initialize();
    } catch (error) {
      print('Error initializing cameras: $error');
      return [];
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Camera Screen'),
      ),
      body: FutureBuilder(
        future: _cameraInitialization,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(child: CircularProgressIndicator());
          } else if (snapshot.hasError) {
            return Center(child: Text('Error initializing cameras'));
          } else {
            List cameras = snapshot.data as List;
            return MaskForCameraView(
              visiblePopButton: false,
              boxBorderWidth: 2.6,
              boxHeight: 1300,
              boxWidth: 1013,
              cameraDescription: cameras.isNotEmpty ? cameras.first : null,
              onTake: (MaskForCameraViewResult? result) {
                if (result != null) {}
              },
            );
          }
        },
      ),
    );
  }
}

Future<void> handleCameraResult(
    BuildContext context, MaskForCameraViewResult result) async {
  showModalBottomSheet(
    context: context,
    isScrollControlled: true,
    backgroundColor: Colors.transparent,
    builder: (context) => Container(
      padding: const EdgeInsets.symmetric(
        vertical: 12.0,
        horizontal: 14.0,
      ),
      decoration: const BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(26.0),
          topRight: Radius.circular(26.0),
        ),
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          const Text(
            "Cropped Images",
            style: TextStyle(
              fontSize: 24.0,
              fontWeight: FontWeight.w600,
            ),
          ),
          const SizedBox(height: 12.0),
          result.croppedImage != null
              ? MyImageView(imageBytes: result.croppedImage!)
              : Container(),
          const SizedBox(height: 8.0),
          const SizedBox(height: 20.0),
          Container(
            height: 48.0,
            decoration: BoxDecoration(
              color: Colors.purple,
              borderRadius: BorderRadius.circular(12.0),
            ),
            child: Material(
              color: Colors.transparent,
              child: InkWell(
                onTap: () {
                  launch("https://pub.dev/packages/mask_for_camera_view");
                },
                borderRadius: BorderRadius.circular(12.0),
                child: const Center(
                  child: Text(
                    "Find plugin on pub.dev",
                    style: TextStyle(
                      fontSize: 16.0,
                      fontWeight: FontWeight.w600,
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    ),
  );
}

class MyImageView extends StatelessWidget {
  const MyImageView({Key? key, required this.imageBytes}) : super(key: key);

  final Uint8List imageBytes;

  @override
  Widget build(BuildContext context) {
    savePicture(imageBytes);
    return ClipRRect(
      borderRadius: BorderRadius.circular(4.0),
      child: SizedBox(
        width: double.infinity,
        child: Image.memory(imageBytes),
      ),
    );
  }
}

Future<void> savePicture(Uint8List imageBytes) async {
  Directory appDocuments = await getApplicationDocumentsDirectory();
  String appDocumentsPath = appDocuments.path;
  String filePath = '$appDocumentsPath/imageTmp.png';
  String base64Image = base64Encode(imageBytes);
  print(base64Image);

  final decodeBytes = base64Decode(base64Image);
  var file = File(filePath);
  file.writeAsBytesSync(decodeBytes);

  print(file.toString());

  final textRecognizer = TextRecognizer();
  var ocrFile = _imageToInputImage(file);
  final recognizedText = await textRecognizer.processImage(ocrFile);
  print(recognizedText.text);
}

InputImage _imageToInputImage(File imageFile) {
  return InputImage.fromFilePath(imageFile.path);
}
