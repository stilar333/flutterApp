import 'dart:convert';

import 'dart:io';
import 'dart:typed_data';
import 'package:connectivity/connectivity.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_login_layout/components/splitsql.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class FetchDataAndPost {
  Future<List<int>> imageToByte(File medidorImageFile) async {
    try {
      final int blockSize = 1024;
      List<int> bytes = [];

      // Abrir el archivo en modo de solo lectura
      RandomAccessFile file = await medidorImageFile.open(mode: FileMode.read);

      int length = await file.length();
      int offset = 0;

      while (offset < length) {
        int chunkSize =
            offset + blockSize < length ? blockSize : length - offset;
        List<int> chunk = await file.read(chunkSize);
        bytes.addAll(chunk);
        offset += chunkSize;
      }

      await file.close();

      Uint8List iByte = Uint8List.fromList(bytes);

      print(iByte);
      return iByte;
    } catch (e) {
      print('Error al convertir la imagen: $e');
      return [];
    }
  }

  SplitSql splitsql = SplitSql();
  Future<List<String>> fetchDataFromEndpoint(String idCliente) async {
    try {
      var headers = {'Content-Type': 'application/x-www-form-urlencoded'};
      var data = {
        'tipo': '3',
        'emp': 'GDR101105CS0',
        'suc': 'Matriz',
        'usu': 'gasdiamante08@gmail.com',
        'pas': '1234',
        'cns':
            'SELECT c.cliente, c.rfcCliente, c.razonSocial, a.almacen as condominio, d.noInterior as departamento\nFROM DbCliente as c INNER JOIN DbCliente_A as a ON a.keyAncestor = c.key\nLEFT JOIN DbClienteDireccion as d ON d.empresa = c.empresa AND d.cliente = c.cliente AND d.moneda = c.moneda\nWHERE c.empresa = \'GDR101105CS0\' and c.cliente = $idCliente and d.tipo = 1'
      };

      var dio = Dio();
      var response = await dio.request(
        'http://getpost.si-nube.appspot.com/getpost',
        options: Options(
          method: 'POST',
          headers: headers,
        ),
        data: data,
      );

      if (response.statusCode == 200) {
        // Utilizar la función inputString para procesar la respuesta
        List<String> result = SplitSql.inputString(response.data);
        print('Respuesta exitosa: $result');
        // Convertir la lista a un Map si es necesario
        return result;
      } else {
        print("error else");
        return [];
      }
    } catch (e) {
      print("eror catch");
      return [];
    }
  }

  Future<void> postFormBackup(BuildContext context, data) async {
    try {
      var uri = Uri.parse('https://srv426423.hstgr.cloud:3000/backups/create');

      // Configuración para deshabilitar la verificación de certificados SSL
      HttpClient httpClient = HttpClient();
      httpClient.badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
      HttpClientRequest request = await httpClient.postUrl(uri);

      // Configurar encabezados y cuerpo de la solicitud
      request.headers.set('Content-Type', 'application/json');
      request.write(data);

      // Obtener la respuesta
      HttpClientResponse response = await request.close();
      String responseBody = await response.transform(utf8.decoder).join();
      print(response.statusCode);
      print(responseBody);

      httpClient.close(); // Cierra el cliente HttpClient después de su uso.
    } catch (e) {
      print('Error: $e');
    }
  }

  Future<void> postFromSinube(context, lstString, lstLong) async {
    var data = jsonEncode({"lstString": lstString, "lstLong": lstLong});
    //print(lstString);
    //print(lstLong);
    try {
      var uri = Uri.parse(
          'https://ep-dot-gas-sinube.appspot.com/api/v1/lectura_agregar/GDR101105CS0/Matriz/gasdiamante08@gmail.com/1234');

      // Configuración para deshabilitar la verificación de certificados SSL
      HttpClient httpClient = HttpClient();
      httpClient.badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
      HttpClientRequest request = await httpClient.postUrl(uri);

      // Configurar encabezados y cuerpo de la solicitud
      request.headers.set('Content-Type', 'application/json');
      request.write(data);

      // Obtener la respuesta
      HttpClientResponse response = await request.close();
      String responseBody = await response.transform(utf8.decoder).join();
      //print(response.statusCode);
      print(responseBody);

      if (response.statusCode == 200) {
        print("SE GUARDO EN SIN UBE");
        ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(
            content: Text('La información se guardó en sinube.'),
            backgroundColor: Colors.green,
            duration: Duration(seconds: 4),
          ),
        );
      } else {
        print(response.reasonPhrase);

        Map<String, dynamic> jsonResponse = json.decode(responseBody);
        String errorMessage = jsonResponse["error"]["message"];

        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text(errorMessage),
            backgroundColor: Colors.red,
            duration: Duration(seconds: 6),
          ),
        );
      }

      httpClient.close();
    } catch (e) {
      print('Error: $e');
    }
  }

  Future<void> getDataFromSQLite(String complex) async {
    // Path donde se copiará la base de datos en el dispositivo
    try {
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "mirage81");
      print("Ruta del archivo: $path");
      // Abre la base de datos
      Database database = await openDatabase(path, readOnly: true);

      // Consulta los datos
      List<Map<String, dynamic>> result;
      if (complex.isEmpty) {
        result = await database.rawQuery("SELECT * FROM Customers");
      } else {
        result = await database
            .rawQuery("SELECT * FROM Customers WHERE Id=?", [complex]);
      }

      result.forEach((row) {
        print("ID: ${row['Id']}, Depto: ${row['Depto']}");
      });

      // Cierra la base de datos
      await database.close();
    } on Exception catch (e) {
      print(e);
    }
  }
}
