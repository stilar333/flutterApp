import 'package:dio/dio.dart';
import 'package:flutter_login_layout/components/splitsql.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

class LocalDatabase {
  late Database _database;

  Future<void> initDatabasecreate() async {
    _database = await openDatabase(
      join(await getDatabasesPath(), 'customer21.db'),
      version: 1,
    );
  }

  Future<void> initDatabase() async {
    bool databaseExists = await doesDatabaseExist();

    if (!databaseExists) {
      _database = await openDatabase(
        join(await getDatabasesPath(), 'customer21.db'),
        onCreate: (db, version) async {
          print("se crea la db");
          try {
            await db.execute(
              "CREATE TABLE Customer ("
              "id TEXT,"
              "rfc TEXT,"
              "cliente TEXT,"
              "condominio TEXT,"
              "departamento TEXT,"
              "periodo1 TEXT,"
              "razonSocial TEXT"
              ")",
            );

            print("consultando sin nube");
          } catch (e) {
            print('Error al crear la db: $e');
          }
        },
        version: 1,
      );

      for (int next = 1; next <= 11859; next++) {
        try {
          List<String> result = await fetchDataFromEndpoint(next);

          if (result.isNotEmpty) {
            Map<String, dynamic> data = {
              'id': result[12],
              'rfc': result[12],
              'cliente': result[13],
              'condominio': result[14],
              'departamento': result[15],
              'periodo1': obtenerUltimosDosDigitosYMes(),
              'razonSocial': result[13],
            };

            await insertData(data);
          }
        } catch (e) {
          print("Error en la inserción en SQLite: $e");
        }
      }
    }
  }

  Future<bool> doesDatabaseExist() async {
    String path = join(await getDatabasesPath(), 'customer21.db');
    return databaseFactory.databaseExists(path);
  }

  Future<void> insertData(Map<String, dynamic> data) async {
    try {
      await _database.insert(
        'Customer',
        data,
        conflictAlgorithm: ConflictAlgorithm.replace,
      );
      print("incertado en tu sql");
    } catch (e) {
      print("NI IDEA PERO NO SE INSERTO " + e.toString());
    }
  }

  Future<List<Map<String, dynamic>>> retrieveData() async {
    return await _database.query('Customer');
  }

  Future<Map<String, dynamic>?> getDataById(String id) async {
    try {
      List<Map<String, dynamic>> result = await _database.query(
        'Customer',
        where: 'id = ?',
        whereArgs: ["String¬$id"],
      );

      if (result.isNotEmpty) {
        return result.first;
      } else {
        print('No se encontraron datos con el ID: $id');
        return {
          "id": "no existe",
          "rfc": "no existe",
          "id1": "no existe",
          "rfc2": "no existe",
          "id3": "no existe",
          "rfc4": "no existe",
          "id5": "no existe",
          "rfc7": "no existe",
        };
      }
    } catch (e) {
      print('Error al obtener datos por ID: $e');
      return {
        "id": "no existe",
        "rfc": "no existe",
        "id1": "no existe",
        "rfc2": "no existe",
        "id3": "no existe",
        "rfc4": "no existe",
        "id5": "no existe",
        "rfc7": "no existe",
      };
    }
  }

  Future<List<String>> fetchDataFromEndpoint(int next) async {
    try {
      // Tu código para realizar la solicitud al endpoint con el valor de 'next'

      // Puedes utilizar el paquete Dio para realizar la solicitud POST

      var headers = {'Content-Type': 'application/x-www-form-urlencoded'};
      var data = {
        'tipo': '3',
        'emp': 'GDR101105CS0',
        'suc': 'Matriz',
        'usu': 'gasdiamante08@gmail.com',
        'pas': '1234',
        'cns':
            "SELECT c.cliente, c.rfcCliente, c.razonSocial, a.almacen as condominio, d.noInterior as departamento FROM DbCliente as c INNER JOIN DbCliente_A as a ON a.keyAncestor = c.key LEFT JOIN DbClienteDireccion as d ON d.empresa = c.empresa AND d.cliente = c.cliente AND d.moneda = c.moneda WHERE c.empresa = 'GDR101105CS0' and c.cliente = $next and d.tipo = 1",
      };

      var dio = Dio();
      var response = await dio.request(
        'http://getpost.si-nube.appspot.com/getpost',
        options: Options(
          method: 'POST',
          headers: headers,
        ),
        data: data,
      );

      if (response.statusCode == 200) {
        List<String> result = SplitSql.inputString(response.data);
        print('Respuesta exitosa: $result');
        return result;
      } else {
        print(
            'Error en la solicitud: ${response.statusCode} + ${response.data}');
        return [];
      }
    } catch (e) {
      print('Error en la solicitud: $e');
      return [];
    }
  }

  String obtenerUltimosDosDigitosYMes() {
    var currDt = DateTime.now();
    var ultimosDosDigitosAno = currDt.year;
    var mes = currDt.month.toString().padLeft(2, '0');

    return '$ultimosDosDigitosAno$mes';
  }
}
