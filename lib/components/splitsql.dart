//3|&NullSiNube;|cliente|Long|rfcCliente|String|razonSocial|String|condominio|String|departamento|String¬237|XAXX010101000|BIANKA IVONNE SALAZAR TELLEZ|QUEMADA|102
import 'dart:collection';

import 'package:intl/intl.dart';

class SplitSql {
  static List<String> inputString(String sql) {
    String strSQL = sql;
    final splitted = strSQL.split('|');
    print(splitted); // [Hello, world!];
    return splitted;
  }
}

// Use
/*
List<String> result = SplitSql.inputString("3|&NullSiNube;|cliente|Long|rfcCliente|String|razonSocial|String|condominio|String|departamento|String¬237|XAXX010101000|BIANKA IVONNE SALAZAR TELLEZ|QUEMADA|102");
print(result[12].toString()); RFC
print(result[13].toString()); NOMBRE
print(result[14].toString()); CONDOMINIO
print(result[15].toString()); DEPARTAMENTO

 */