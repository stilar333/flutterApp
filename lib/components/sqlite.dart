import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class SqlLite {
  Future<Database> initDatabase() async {
    return openDatabase(
      join(await getDatabasesPath(), 'datos.db'),
      onCreate: (db, version) {
        return db.execute('''
          CREATE TABLE datos(
            idcliente TEXT PRIMARY KEY,
            cliente TEXT,
            departamento TEXT,
            rfc TEXT,
            condominio TEXT,
            razonsocial TEXT,
            periodo1 INTEGER,
            codigomedidor INTEGER,
            nombreImagen TEXT
          )
        ''');
      },
      version: 1,
    );
  }

  Future<List<Map<String, dynamic>>> getAllData() async {
    final db = await initDatabase();
    List<Map<String, dynamic>> result = await db.query('datos');
    return result;
  }

  Future<void> deleteDataById(int id) async {
    final db = await initDatabase();
    await db.delete(
      'datos',
      where: 'idcliente = ?',
      whereArgs: [id],
    );
  }

  Future<void> recuperarDatos() async {
    final Database db = await initDatabase();

    final List<Map<String, dynamic>> datos = await db.query('datos');

    for (var dato in datos) {
      print(
          'idcliente: ${dato['idcliente']}, cliente: ${dato['cliente']}, departamento: ${dato['departamento']}, rfc: ${dato['rfc']}, condominio: ${dato['condominio']}, razonsocial: ${dato['razonsocial']}, periodo1: ${dato['periodo1']}, codigomedidor: ${dato['codigomedidor']}, nombreImagen: ${dato['nombreImagen']}');
    }
  }
}
