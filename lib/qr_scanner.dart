import 'dart:async';
import 'dart:convert';
import 'dart:typed_data';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter_login_layout/QRScannerScreen.dart';
import 'package:flutter_login_layout/components/fetchdata.dart';
import 'package:flutter_login_layout/components/splitsql.dart';
import 'package:flutter_login_layout/components/sqlNube.dart';
import 'package:flutter_login_layout/components/sqlite.dart';
import 'package:flutter_login_layout/widgets/mask.dart';
import 'package:ndialog/ndialog.dart';
import 'package:sqflite/sqflite.dart';

class FormData {
  String idCliente = '';
  String condominio = '';
  String departamento = '';
  String cliente = '';
  String rfc = '';
  String razonSocial = '';
  String periodo1 = '';
  Uint8List? medidorImage;
  String codigoMedidor = '';
}

class QRCodeWidget extends StatefulWidget {
  final String? code;

  QRCodeWidget(this.code);

  @override
  State<QRCodeWidget> createState() => _QRCodeWidgetState();
}

class _QRCodeWidgetState extends State<QRCodeWidget> {
  SqlLite sql = SqlLite();
  FormData formData = FormData();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final FetchDataAndPost fetchdata = FetchDataAndPost();
  String result = "";
  FetchDataAndPost test = FetchDataAndPost();
  SplitSql splitsql = SplitSql();
  TextEditingController idClienteController = TextEditingController();
  TextEditingController condominioController = TextEditingController();
  TextEditingController departamentoController = TextEditingController();
  TextEditingController clienteController = TextEditingController();
  TextEditingController rfcController = TextEditingController();
  TextEditingController razonSocialController = TextEditingController();
  TextEditingController periodo1Controller = TextEditingController();
  TextEditingController codigoMedidorController = TextEditingController();

  @override
  initState() {
    super.initState();

    _initsql();
    if (widget.code != "") {
      _FetchForm(widget.code);
    }
  }

  @override
  void dispose() {
    super.dispose();

    idClienteController.dispose();
    condominioController.dispose();
    departamentoController.dispose();
    clienteController.dispose();
    rfcController.dispose();
    razonSocialController.dispose();
    periodo1Controller.dispose();
    codigoMedidorController.dispose();
  }

  String filterNumbers(String input) {
    return input.replaceAll(RegExp(r'[^0-9]'), '');
  }

  String obtenerUltimosDosDigitosYMes() {
    var currDt = DateTime.now();
    var ultimosDosDigitosAno = currDt.year;
    var mes = currDt.month.toString().padLeft(2, '0');

    return '$ultimosDosDigitosAno$mes';
  }

  Future<Map<String, dynamic>?> _fetchsql(String id) async {
    LocalDatabase localDatabase = LocalDatabase();
    await localDatabase.initDatabasecreate();

    var map = await localDatabase.getDataById(id);
    print(map);
    return map;
  }

  Future<void> _initsql() async {
    LocalDatabase localDatabase = LocalDatabase();

    await localDatabase.initDatabase();
    await _syncData();
  }

  Future<void> _FetchForm(id) async {
    try {
      var connectivityResult = await Connectivity().checkConnectivity();

      if (connectivityResult == ConnectivityResult.mobile ||
          connectivityResult == ConnectivityResult.wifi) {
        print("CON INTERTER");
        var resultadoFetch = await fetchdata.fetchDataFromEndpoint(id);

        setState(() {
          formData.rfc = resultadoFetch[12].toString();
          formData.cliente = resultadoFetch[13].toString();
          formData.condominio = resultadoFetch[14].toString();
          formData.departamento = resultadoFetch[15].toString();
          formData.periodo1 = obtenerUltimosDosDigitosYMes();
          formData.razonSocial = resultadoFetch[13].toString();
          idClienteController.text = id;
          rfcController.text = resultadoFetch[12].toString();
          periodo1Controller.text = obtenerUltimosDosDigitosYMes();
          clienteController.text = resultadoFetch[13].toString();
          condominioController.text = resultadoFetch[14].toString();
          departamentoController.text = resultadoFetch[15].toString();
          razonSocialController.text = resultadoFetch[13].toString();
        });

        WidgetsBinding.instance!.addPostFrameCallback((_) {
          setState(() {});
        });
      } else {
        /* print("SIN INTERTER");
        var resultadoFetch = await _fetchsql(id);
        setState(() {
          if (resultadoFetch != null) {
            formData.rfc = resultadoFetch["rfc"];
            formData.cliente = resultadoFetch["cliente"].toString();
            formData.condominio = resultadoFetch["condominio"].toString();
            formData.departamento = resultadoFetch["departamento"].toString();
            formData.periodo1 = obtenerUltimosDosDigitosYMes();
            formData.razonSocial = resultadoFetch["razonSocial"].toString();
            idClienteController.text = id;
            rfcController.text = resultadoFetch["rfc"].toString();
            periodo1Controller.text = obtenerUltimosDosDigitosYMes();
            clienteController.text = resultadoFetch["cliente"].toString();
            condominioController.text = resultadoFetch["condominio"].toString();
            departamentoController.text =
                resultadoFetch["departamento"].toString();
            razonSocialController.text =
                resultadoFetch["razonSocial"].toString();
          }
        });
        print(resultadoFetch);
        print("Se busco sin internet");
        WidgetsBinding.instance!.addPostFrameCallback((_) {
          setState(() {});
        }); */
      }
    } catch (e) {
      print("NO SE CUAL DASHDHASDHDA" + e.toString());
    }
  }

  Future<void> _submitForm() async {
    if (formData.medidorImage == null) {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          backgroundColor: Colors.red,
          content: Text('Porfavor carga una imagen del medidor.'),
          duration: Duration(seconds: 2),
        ),
      );
      return;
    }

    var data = json.encode({
      "idcliente": formData.idCliente,
      "period": formData.periodo1,
      "rfc": formData.rfc,
      "cliente": formData.cliente,
      "departamento": formData.departamento,
      "condominio": formData.condominio,
      "medidor": formData.codigoMedidor,
      "razonsocial": formData.razonSocial
    });

    List datanube = [
      formData.periodo1,
      idClienteController.text,
      formData.condominio,
      formData.departamento,
      formData.rfc,
      formData.razonSocial,
      formData.codigoMedidor
    ];

    Uint8List? medidorImage = formData.medidorImage;

    if (medidorImage != null) {
      _clearForm();
      await fetchdata.postFormBackup(context, data);
      await fetchdata.postFromSinube(context, datanube, medidorImage);
    }
  }

  void _clearForm() {
    idClienteController.clear();
    condominioController.clear();
    departamentoController.clear();
    clienteController.clear();
    rfcController.clear();
    razonSocialController.clear();
    periodo1Controller.clear();
    codigoMedidorController.clear();

    setState(() {
      formData.medidorImage = null;
    });
  }

  Future<void> _syncData() async {
    var connectivityResult = await Connectivity().checkConnectivity();

    if (connectivityResult == ConnectivityResult.mobile ||
        connectivityResult == ConnectivityResult.wifi) {
      // Obtener datos almacenados localmente
      List<Map<String, dynamic>> localData = await sql.getAllData();
      ProgressDialog progressDialog = ProgressDialog(
        context,
        blur: 10,
        dismissable: false,
        title: Text("Se estan sincronizando los datos"),
        message: Text("Por favor espera un momento"),
      );
      // Sincronizar datos con el servidor
      progressDialog.show();
      for (var data in localData) {
        var data2 = json.encode({
          "idcliente": data["idcliente"],
          "period": data["periodo1"],
          "rfc": data["rfc"],
          "cliente": data["cliente"],
          "departamento": data["departamento"],
          "condominio": data["condominio"],
          "medidor": data["codigomedidor"],
          "razonsocial": data["razonsocial"]
        });

        List datanube = [
          data["periodo1"],
          data["idcliente"],
          data["condominio"],
          data["departamento"],
          data["rfc"],
          data["razonsocial"],
          data["codigomedidor"]
        ];

        print(data2);
        await _syncSingleData(data2, datanube, data["nombreImagen"], data);
      }
      progressDialog.dismiss();
    }
  }

  Future<void> _syncSingleData(String data2, List datanube, medidorImage,
      Map<String, dynamic> data) async {
    try {
      await fetchdata.postFormBackup(context, data2);
      await fetchdata.postFromSinube(context, datanube, medidorImage);
      await sql.deleteDataById(data['idcliente']);
    } catch (e) {
      print("Error al sincronizar el dato: $e");
    }
  }

  Future<void> insertarDatos() async {
    final Database db = await sql.initDatabase();
    String idCliente;
    try {
      idCliente = idClienteController.text;
    } catch (e) {
      print('Error:  id cliente ser un número entero.');
      return;
    }
    String periodo;
    try {
      periodo = periodo1Controller.text;
    } catch (e) {
      print('Error: periodo ser un número entero.');
      return;
    }
    String codigomedidor;
    try {
      codigomedidor = codigoMedidorController.text;
    } catch (e) {
      print('Error: codigo medidor  ser un número entero.');
      return;
    }
    String nombre =
        clienteController.text.replaceAll(RegExp(r'[^a-zA-Z0-9 ]'), '');
    String rfc = rfcController.text;

    await db.insert(
      'datos',
      {
        'idcliente': idCliente,
        'cliente': nombre,
        'departamento': departamentoController.text,
        'rfc': rfc,
        'condominio': condominioController.text,
        'razonsocial': razonSocialController.text,
        'periodo1': periodo,
        'codigomedidor': codigomedidor,
        "nombreImagen": formData.medidorImage,
      },
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
    ScaffoldMessenger.of(context).showSnackBar(
      const SnackBar(
        content: Text('La información se guardó En el dispositivo.'),
        backgroundColor: Colors.green,
        duration: Duration(seconds: 4),
      ),
    );
    print('Datos insertados correctamente.');
    sql.recuperarDatos();
  }

  Future<void> _openQRScanner() async {
    try {
      await Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => const QRScannerScreen()),
      );
    } catch (e) {
      print('Error en la navegación: $e');
    }
  }

  @override
  Widget build(BuildContext context) {
    print("Reconstruyendo el widget");
    return Scaffold(
      appBar: AppBar(
        title: Text("QR Escaner"),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Form(
            key: _formKey,
            child: Column(
              children: [
                ElevatedButton(
                  onPressed: _openQRScanner,
                  child: Text("Escanear QR"),
                ),
                TextFormField(
                  controller: idClienteController,
                  decoration: InputDecoration(labelText: 'ID Cliente'),
                  onChanged: (value) async {
                    formData.idCliente = value.toString();
                    await _FetchForm(value.toString());
                  },
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Este campo es obligatorio';
                    }
                    return null;
                  },
                ),
                TextFormField(
                  controller: condominioController,
                  decoration: InputDecoration(labelText: 'Condominio'),
                  onChanged: (value) {
                    formData.condominio = value;
                  },
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Este campo es obligatorio';
                    }
                    return null;
                  },
                ),
                TextFormField(
                  controller: departamentoController,
                  decoration: InputDecoration(labelText: 'Departamento'),
                  onChanged: (value) {
                    formData.departamento = value;
                  },
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Este campo es obligatorio';
                    }
                    return null;
                  },
                ),
                TextFormField(
                  controller: clienteController,
                  decoration: InputDecoration(labelText: 'Cliente'),
                  onChanged: (value) {
                    formData.cliente = value;
                  },
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Este campo es obligatorio';
                    }
                    return null;
                  },
                ),
                TextFormField(
                  controller: rfcController,
                  decoration: InputDecoration(labelText: 'RFC'),
                  onChanged: (value) {
                    formData.rfc = value;
                  },
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Este campo es obligatorio';
                    }
                    return null;
                  },
                ),
                TextFormField(
                  controller: razonSocialController,
                  decoration: InputDecoration(labelText: 'Razon Social'),
                  onChanged: (value) {
                    formData.razonSocial = value;
                  },
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Este campo es obligatorio';
                    }
                    return null;
                  },
                ),
                TextFormField(
                  controller: periodo1Controller,
                  decoration:
                      InputDecoration(labelText: 'Periodo (formato: "AAAAMM")'),
                  onChanged: (value) {
                    formData.periodo1 = value;
                  },
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Este campo es obligatorio';
                    }
                    return null;
                  },
                ),
                ElevatedButton(
                  onPressed: () async {
                    Map<String, dynamic>? result = await Navigator.of(context)
                        .push(MaterialPageRoute(
                            builder: (context) => MaskWidget(cameras)));

                    if (result != null) {
                      Uint8List? croppedImage = result['croppedImage'];
                      String recognizedText = result['recognizedText'];
                      setState(() {
                        formData.medidorImage = croppedImage;
                        formData.codigoMedidor = filterNumbers(recognizedText);
                        codigoMedidorController.text =
                            filterNumbers(recognizedText);
                      });
                    }
                  },
                  child: Text("Tomar foto del medidor"),
                ),
                if (formData.medidorImage != null)
                  Container(
                    height: 500,
                    width: 600,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: MemoryImage(formData.medidorImage!),
                        fit: BoxFit.contain,
                      ),
                    ),
                  ),
                TextFormField(
                  controller: codigoMedidorController,
                  decoration: InputDecoration(labelText: 'Codigo del medidor'),
                  onChanged: (value) {
                    formData.codigoMedidor = value;
                  },
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Este campo es obligatorio';
                    }
                    return null;
                  },
                ),
                SizedBox(height: 16),
                ElevatedButton(
                  onPressed: () async {
                    if (_formKey.currentState!.validate()) {
                      var connectivityResult =
                          await Connectivity().checkConnectivity();

                      if (connectivityResult == ConnectivityResult.mobile ||
                          connectivityResult == ConnectivityResult.wifi) {
                        _submitForm();
                      } else {
                        await insertarDatos();
                      }
                    }
                  },
                  child: Text("Subir información"),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
