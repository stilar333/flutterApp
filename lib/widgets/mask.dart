import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';
import 'package:flutter/material.dart';
import 'package:google_mlkit_text_recognition/google_mlkit_text_recognition.dart';
import 'package:mask_for_camera_view/inside_line/mask_for_camera_view_inside_line.dart';
import 'package:mask_for_camera_view/mask_for_camera_view.dart';
import 'package:mask_for_camera_view/mask_for_camera_view_result.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:flutter/painting.dart';
import 'package:path_provider/path_provider.dart';
import 'dart:io' as Io;

late List<CameraDescription> cameras;

class MaskWidget extends StatefulWidget {
  MaskWidget(List<CameraDescription> cams) {
    cameras = cams;
    print(cameras);
  }

  @override
  _MaskWidgetState createState() => _MaskWidgetState();
}

class _MaskWidgetState extends State<MaskWidget> {
  @override
  Widget build(BuildContext context) {
    return MaskForCameraView(
      visiblePopButton: false,
      insideLine: MaskForCameraViewInsideLine(
        position: MaskForCameraViewInsideLinePosition.center,
        direction: MaskForCameraViewInsideLineDirection.horizontal,
      ),
      boxBorderWidth: 3,
      boxHeight: 1800,
      boxWidth: 1313,
      cameraDescription: cameras.first,
      onTake: (MaskForCameraViewResult? res) async {
        if (res != null) {
          final recognizedText = await processImage(res.firstPartImage);
          Navigator.of(context).pop({
            'croppedImage': res.firstPartImage,
            'recognizedText': recognizedText,
          });
        }
      },
    );
  }

  Future<String> processImage(Uint8List? croppedImage) async {
    if (croppedImage == null) return '';

    final textRecognizer = TextRecognizer();
    var croppedFile = await uint8ListToFile(croppedImage);
    var ocrFile = _imageToInputImage(croppedFile);
    final recognizedText = await textRecognizer.processImage(ocrFile);

    print('Texto reconocido: ${recognizedText.text}');

    return recognizedText.text;
  }

  InputImage _imageToInputImage(File imageFile) {
    return InputImage.fromFilePath(imageFile.path);
  }

  Future<File> uint8ListToFile(Uint8List uint8List) async {
    Directory tempDir = await getTemporaryDirectory();
    String tempPath = tempDir.path;

    File file = File('$tempPath/${DateTime.now().millisecondsSinceEpoch}.png');
    await file.writeAsBytes(uint8List);

    return file;
  }
}
