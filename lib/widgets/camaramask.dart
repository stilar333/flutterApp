import 'dart:io';

import 'package:camera/camera.dart';
import 'package:flutter/material.dart';

class CameraWithMask extends StatefulWidget {
  @override
  _CameraWithMaskState createState() => _CameraWithMaskState();
}

class _CameraWithMaskState extends State<CameraWithMask> {
  late CameraController _cameraController;
  late List<CameraDescription> _cameras;
  bool _isCameraInitialized = false;

  
  @override
  void initState() {
    super.initState();
    _initializeCamera();
  }

    Future<void> _initializeCamera() async {
    try {
      _cameras = await availableCameras();
      _cameraController = CameraController(
        _cameras.first,
        ResolutionPreset.medium,
      );

      await _cameraController.initialize();

      if (mounted) {
        setState(() {
          _isCameraInitialized = true;
        });
      }
    } catch (e) {
      print('Error al inicializar la cámara: $e');
    }
  }

  @override
  void dispose() {
    _cameraController.dispose();
    super.dispose();
  }

  Future<File?> takePicture() async {
    try {
      if (!_isCameraInitialized) {
        print('La cámara no está inicializada.');
        return null;
      }

      XFile picture = await _cameraController.takePicture();
      return File(picture.path);
    } catch (e) {
      print('Error al capturar la imagen: $e');
      return null;
    }
  }

  @override
  Widget build(BuildContext context) {
    if (_cameraController.value.isInitialized) {
      return Scaffold(
        body: Stack(
          children: [
            CameraPreview(_cameraController),
            Align(
              alignment: Alignment.center,
              child: Container(
                height: 50, 
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                  color: Colors.black.withOpacity(0.5),
                ),
              ),
            ),
          ],
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () async {
            File? capturedFile = await takePicture();
            Navigator.pop(context, capturedFile); 
          },
          child: Icon(Icons.camera),
        ),
      );
    } else {
      return Container();
    }
  }
}
