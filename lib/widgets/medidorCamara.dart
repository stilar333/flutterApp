import 'dart:io';
import 'package:camera/camera.dart';
import 'package:flutter/material.dart';

class CameraScreen extends StatefulWidget {
  @override
  _CameraScreenState createState() => _CameraScreenState();
}

class _CameraScreenState extends State<CameraScreen> {
  late CameraController _controller;
  late Future<void> _initializeControllerFuture;

  @override
  void initState() {
    super.initState();
    _initializeControllerFuture = _initializeCamera();
  }

  Future<void> _initializeCamera() async {
    final cameras = await availableCameras();
    if (cameras.isEmpty) {
      return;
    }
    final firstCamera = cameras.first;

    _controller = CameraController(
      firstCamera,
      ResolutionPreset.medium,
    );

    await _controller.initialize();
  }

  Future<void> _takePicture() async {
    try {
      await _initializeControllerFuture;

      // Ajusta el formato de la imagen aquí
      await _controller.setFlashMode(FlashMode.auto);
      await _controller.setExposureMode(ExposureMode.auto);

      XFile picture = await _controller.takePicture();

      // Restaura la configuración de la cámara si es necesario
      await _controller.setFlashMode(FlashMode.off);
      await _controller.setExposureMode(ExposureMode.auto);

      // Pasa la imagen de vuelta a la pantalla anterior
      Navigator.pop(context, File(picture.path));
    } catch (e) {
      print('Error al tomar la foto y guardarla: $e');
    }
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FutureBuilder<void>(
        future: _initializeControllerFuture,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            return AspectRatio(
              aspectRatio: _controller.value.aspectRatio,
              child: ClipRect(
                child: Transform.scale(
                  scale: 1 / _controller.value.aspectRatio,
                  child: Center(
                    child: AspectRatio(
                      aspectRatio: 1 / 0.5,
                      child: CameraPreview(_controller),
                    ),
                  ),
                ),
              ),
            );
          } else {
            return Center(child: CircularProgressIndicator());
          }
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _takePicture,
        child: Icon(Icons.camera),
      ),
    );
  }
}
