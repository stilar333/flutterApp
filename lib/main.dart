import 'package:flutter_login_layout/qr_scanner.dart';
import 'package:flutter_login_layout/widgets/mask.dart';
import 'package:mask_for_camera_view/mask_for_camera_view.dart';
import 'package:provider/provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_login_layout/pages/login.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'data_app.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  cameras = await MaskForCameraView.initialize();
  runApp(
    ChangeNotifierProvider(
      create: (context) => AppState(),
      child: const MyApp(),
    ),
  );
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.blue),
        useMaterial3: true,
      ),
      home: FutureBuilder(
        future:
            checkLoginStatus(), // Función para verificar el estado de inicio de sesión
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            if (snapshot.data == true) {
              // Si está iniciado sesión, mostrar el widget del código QR
              return QRCodeWidget('');
            } else {
              // Si no está iniciado sesión, mostrar la pantalla de inicio de sesión
              return const LoginPage();
            }
          } else {
            // Mientras se verifica el estado de inicio de sesión, puedes mostrar un indicador de carga
            return CircularProgressIndicator();
          }
        },
      ),
    );
  }

  Future<bool> checkLoginStatus() async {
    // Aquí deberías implementar la lógica para verificar el estado de inicio de sesión
    // Puedes utilizar shared_preferences u otro método para obtener el estado
    // Este es solo un ejemplo básico, debes adaptarlo según tu implementación
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool('isLoggedIn') ?? false;
  }
}
